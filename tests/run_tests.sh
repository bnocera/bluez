#!/bin/bash

set -ex

# This test is designed such that a Bluetooth controller is not required but we
# can at least check to make sure the commands are installed correctly

# Is bccmd present and doesn't fail if called
#bccmd --help

# Is btattach present and doesn't fail if called
btattach --version

# Is ciptool present and doesn't fail if called
ciptool --help

# hciattach test
any_str=$(hciattach -l | grep -E "^any")
if [ "$any_str" != "any       0x0000,0x0000" ]
then
    echo "Unable to find the 'any' hciattach configuration"
    exit 99
fi

# Is hciconfig present and doesn't fail if called
hciconfig --help

# Is hcitool present and doesn't fail if called
hcitool --help

# Is rfcomm present and doesn't fail if called
rfcomm --help

# Is sdptool present and doesn't fail if called
sdptool --help
